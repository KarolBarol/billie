/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

module.exports = {
  testEnvironment: "jsdom",
  clearMocks: true,
  moduleDirectories: ["node_modules", "src"],
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  snapshotSerializers: ['@emotion/jest/serializer'],
  setupFilesAfterEnv: [
    "<rootDir>/src/setupTests.js",
  ],
  globals: {
    globals: {
      ASYNC_DATA_DELAY: '0'
    }
  },
  testTimeout: 10000,
};
