import React from 'react';
import { css, Global } from '@emotion/react';

export const GlobalStyles: React.FC = () => (
  <Global
    styles={css`
      @font-face {
        font-family: 'Roboto';
        src: url('src/assets/fonts/roboto/Roboto-Regular.ttf')
          format('truetype');
        font-weight: 400;
        font-style: normal;
      }
      @font-face {
        font-family: 'Roboto';
        src: url('src/assets/fonts/roboto/Roboto-Light.ttf') format('truetype');
        font-weight: 300;
        font-style: normal;
      }
      @font-face {
        font-family: 'Roboto';
        src: url('src/assets/fonts/roboto/Roboto-Bold.ttf') format('truetype');
        font-weight: 700;
        font-style: normal;
      }

      * {
        margin: 0;
        padding: 0;
      }

      *,
      *::before,
      *::after {
        box-sizing: inherit;
      }

      html {
        box-sizing: border-box;
        font-size: 62.5%;
        height: 100vh;
      }

      body {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 16px;
        height: inherit;
      }

      #root {
        height: inherit;
      }
    `}
  />
);
