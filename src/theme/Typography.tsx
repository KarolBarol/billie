import { ElementType } from 'react';
import { colors } from '@theme';
import styled from '@emotion/styled';

export const H1 = styled.h1`
  margin: 0;
  font-size: 5.5rem;
  font-weight: bold;
  line-height: 2;
  color: ${colors.primary};
`;

export const H3 = styled.h3`
  margin: 0;
  font-size: 3.2rem;
  font-weight: bold;
  line-height: 1.8;
  color: ${colors.primary};
`;

export const Text = styled.p<{ as?: ElementType<HTMLParagraphElement> }>`
  font-size: 1.6rem;
  line-height: 1.71;
  font-weight: 300;
  color: ${colors.black};
`;
