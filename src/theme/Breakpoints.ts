export interface ThemeBreakPoints {
  sm: string;
  md: string;
  lg: string;
  xl: string;
}

export const bp = {
  sm: '576px',
  md: '768px',
  lg: '992px',
  xl: '1200px',
};
