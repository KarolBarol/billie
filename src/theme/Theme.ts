import {
  useTheme as emotionUseTheme,
  Theme as EmotionTheme,
} from '@emotion/react';

import { colors, bp } from '.';
import { ThemeBreakPoints } from './Breakpoints';
import { ThemeColors } from './Colors';
export interface Theme extends EmotionTheme {
  colors: ThemeColors;
  bp: ThemeBreakPoints;
}

export interface ThemeProp {
  theme?: Theme;
}

export const theme: Theme = {
  colors,
  bp,
};

export const useTheme = () => (emotionUseTheme as () => Theme)();
