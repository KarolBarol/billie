import React from 'react';
import { ThemeProvider as EmotionThemeProvider } from '@emotion/react';

import { GlobalStyles, theme } from '@theme';

export const ThemeProvider: React.FC = ({ children }): JSX.Element => (
  <EmotionThemeProvider theme={theme}>
    <GlobalStyles />
    {children}
  </EmotionThemeProvider>
);
