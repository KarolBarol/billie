export interface ThemeColors {
  black: string;
  grey: string;
  greyLight: string;
  primary: string;
  red: string;
  secondary: string;
  white: string;
}

export const colors: ThemeColors = {
  black: '#000000',
  grey: '#f3f3f3',
  greyLight: '#fcfcfc',
  primary: '#ff4338',
  red: '#ff4057',
  secondary: '#1f2530',
  white: '#ffffff',
};
