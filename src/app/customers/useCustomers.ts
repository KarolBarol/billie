import { useEffect, useState } from 'react';
import { toDECurrencyString } from '@common/utils';

import { Customer, CustomerRaw } from './Customer';
import { CUSTOMERS_MOCK } from './__mocks__/CustomersMock';
import { globals } from '../../../globals';

const { ASYNC_DATA_DELAY } = globals;

export const useCustomers = () => {
  const [customers, setCustomers] = useState<Customer[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const getCustomers = (): Promise<CustomerRaw[]> =>
    new Promise((resolve) =>
      setTimeout(
        () => resolve(CUSTOMERS_MOCK),
        ASYNC_DATA_DELAY ? Number(ASYNC_DATA_DELAY) : 0
      )
    );

  const setCustomer = (data: Customer) =>
    setCustomers((prev) => {
      const customerIdx = prev.findIndex((customer) => customer.id === data.id);
      prev[customerIdx] = data;
      return prev;
    });

  useEffect(() => {
    getCustomers().then((res) => {
      setCustomers(
        res.map(({ budget, budget_spent, ...rest }) => ({
          ...rest,
          budget,
          budget_display_value: toDECurrencyString(budget),
          budget_spent,
          budget_spent_display_value: toDECurrencyString(budget_spent),
          budget_left: budget - budget_spent,
          budget_left_display_value: toDECurrencyString(budget - budget_spent),
        }))
      );
      setIsLoading(false);
    });
  }, []);

  return {
    customers,
    isLoading,
    setCustomer,
  };
};
