import React from 'react';
import { Layout } from '@common/components';
import { ThemeProp } from '@theme';
import styled from '@emotion/styled';

import { CustomersCounter } from './CustomersCounter';
import { CustomersList } from './CustomersList';
import { useCustomers } from './useCustomers';

export const Customers: React.FC = () => {
  const { customers, isLoading, setCustomer } = useCustomers();

  return (
    <Layout title={'Maritians'} description={'List of Customers'}>
      <Container>
        <CustomersCounter
          customersCount={customers?.length ?? 0}
          isLoading={isLoading}
        />
        <CustomersList
          customers={customers}
          isLoading={isLoading}
          setCustomer={(customer) => setCustomer(customer)}
        />
      </Container>
    </Layout>
  );
};

const Container = styled.div<ThemeProp>(
  ({ theme }) => `
  padding: 0 2rem;
  height: 100%;

  @media (min-width: ${theme.bp.lg}) {
    padding: 0 8.6rem;
  }
`
);
