import React from 'react';
import { ThemeProp } from '@theme';
import Skeleton from 'react-loading-skeleton';
import styled from '@emotion/styled';

interface CustomersCounterProps {
  customersCount: number;
  isLoading: boolean;
}

export const CustomersCounter: React.FC<CustomersCounterProps> = ({
  customersCount,
  isLoading,
}) => {
  if (isLoading) {
    return (
      <Container>
        <Skeleton width="12rem" />
      </Container>
    );
  }

  return (
    <Container data-testid="counter">
      <Value>{customersCount}</Value>
      <Label>{` Customer${customersCount === 1 ? ' ' : 's '}`}</Label>
    </Container>
  );
};

const Container = styled.div`
  padding-top: 8rem;
  padding-bottom: 2.4rem;
  font-size: 1.4rem;
`;

const Label = styled.span``;

const Value = styled.span<ThemeProp>(
  ({ theme }) => `
  padding: 0.2rem 0.3rem;
  border-radius: 0.4rem;
  font-weight: 700;
  background-color: ${theme.colors.grey};
`
);
