export interface CustomerRaw {
  budget_spent: number;
  budget: number;
  date_of_first_purchase: string;
  id: number;
  name: string;
}

export interface Customer extends CustomerRaw {
  [key: string]: string | number;
  budget_display_value: string;
  budget_left_display_value: string;
  budget_left: number;
  budget_spent_display_value: string;
}
