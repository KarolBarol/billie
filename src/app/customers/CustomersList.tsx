import React, { useContext, useEffect, useState } from 'react';
import { ModalContext, Table, TableCol } from '@common/components';
import toast from 'react-hot-toast';

import { Customer } from './Customer';
import { CustomersBudgetModal } from './CustomersBudgetModal';

interface ListProps {
  customers: Customer[];
  isLoading: boolean;
  setCustomer: (customer: Customer) => unknown;
}

const cols: TableCol[] = [
  {
    label: 'name',
    propName: 'name',
    width: '28rem',
    maxWidth: '28rem',
  },
  {
    label: 'date of first purchase',
    propName: 'date_of_first_purchase',
    width: '28rem',
    maxWidth: '28rem',
  },
  {
    label: 'budget',
    propName: 'budget_display_value',
    width: '28rem',
    maxWidth: '28rem',
  },
  {
    label: 'budget spent',
    propName: 'budget_spent_display_value',
    width: '28rem',
    maxWidth: '28rem',
  },
  {
    label: 'budget left',
    propName: 'budget_left_display_value',
    maxWidth: '36rem',
  },
];

export const CustomersList: React.FC<ListProps> = ({
  customers,
  isLoading,
  setCustomer,
}) => {
  const { isOpened, toggleIsOpened } = useContext(ModalContext);
  const [pendingItem, setPendingItem] = useState<Customer | null>(null);

  useEffect(() => {
    !isOpened && setPendingItem(null);
  }, [isOpened]);

  return (
    <>
      <Table
        cols={cols}
        data={customers}
        isLoading={isLoading}
        onRowClick={({ rowIdx }) => {
          setPendingItem({ ...customers[rowIdx] });
          toggleIsOpened();
        }}
      />

      {pendingItem && (
        <CustomersBudgetModal
          customer={pendingItem}
          onSubmit={(customer) => {
            setCustomer(customer);
            toast.success('Budget has been successfully updated!', {
              ariaProps: {
                role: 'status',
                'aria-live': 'polite',
              },
            });
          }}
        />
      )}
    </>
  );
};
