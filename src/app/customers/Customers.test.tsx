/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react';
import {
  render,
  waitFor,
  screen,
  within,
  fireEvent,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TestProvider, mockMatchMedia } from '@common/tests';

import { Customers } from './Customers';

const performRender = () =>
  render(
    <TestProvider>
      <Customers />
    </TestProvider>
  );

const getCounter = () => screen.queryByTestId('counter');

const getCustomerRows = () => screen.queryAllByRole('row');

const getCustomerRowCells = (idx: number) =>
  within(getCustomerRows()[idx]).queryAllByRole('cell');

const getModal = () =>
  screen.queryByRole('dialog', { name: 'Available Budget' });

const getBudgetValueInput = () => screen.queryByRole('spinbutton');

const getSubmitButton = () => screen.queryByRole('button', { name: 'Submit' });

const getCancelButton = () => screen.queryByRole('button', { name: 'Cancel' });

const getErrorMessage = () => screen.findByRole('alert');

const getOverlay = () => screen.queryByTestId('modal--overlay');

const getSuccessToast = () => screen.queryByRole('status');

describe('Customers', () => {
  beforeAll(() => mockMatchMedia(window));

  describe('Displaying a list of customers', () => {
    test('should render a customers counter', async () => {
      performRender();

      // wait for list to be loaded
      await waitFor(() => expect(getCounter()).toBeInTheDocument());

      expect(getCounter()?.textContent).toContain('3 Customers');
    });

    test('should render a list of customers', async () => {
      performRender();

      // wait for list to be loaded
      await waitFor(() => expect(getCounter()).toBeInTheDocument());

      expect(getCustomerRows().length).toBe(4);
      // check if budget values are presented in a german format
      expect(getCustomerRowCells(1)[2].textContent).toContain('10.000,00');
      expect(getCustomerRowCells(1)[3].textContent).toContain('4.500,00');
      expect(getCustomerRowCells(1)[4].textContent).toContain('5.500,00');
    });
  });

  describe("Updating customer's budget", () => {
    test('should persist a new budget value when the user clicks submit', async () => {
      performRender();

      // wait for list to be loaded
      await waitFor(() => expect(getCounter()).toBeInTheDocument());

      // open modal
      fireEvent.click(getCustomerRowCells(1)[1]);
      expect(screen.queryByTestId('body-portal')).toBeInTheDocument();
      await waitFor(() => expect(getModal()).toBeInTheDocument());

      // check if budget value input has focus
      expect(getBudgetValueInput()!).toHaveFocus();

      // provide a new budget value
      userEvent.clear(getBudgetValueInput()!);
      userEvent.type(getBudgetValueInput()!, '20000');

      // submit
      expect(getSubmitButton()).toBeEnabled();
      userEvent.click(getSubmitButton()!);

      // // wait for success toast
      await waitFor(() => expect(getSuccessToast()).toBeInTheDocument());

      // wait for modal to disappear
      await waitFor(() => expect(getModal()).not.toBeInTheDocument());

      // verify if the change has been reflected
      expect(getCustomerRowCells(1)[2].textContent).toContain('20.000,00');
      expect(getCustomerRowCells(1)[4].textContent).toContain('15.500,00');
    });

    test('should disable submit action when a new budget value lower then value spent', async () => {
      performRender();

      // wait for list to be loaded
      await waitFor(() => expect(getCounter()).toBeInTheDocument());

      // open modal
      fireEvent.click(getCustomerRowCells(1)[1]);
      expect(screen.queryByTestId('body-portal')).toBeInTheDocument();
      await waitFor(() => expect(getModal()).toBeInTheDocument());

      // provide a new budget value lower then budget spent
      userEvent.clear(getBudgetValueInput()!);
      userEvent.type(getBudgetValueInput()!, '4000');

      // verify if it's not possible to persist value
      expect(await getErrorMessage()).toBeInTheDocument();
      expect(getSubmitButton()).toBeDisabled();
    });

    test('should not persist value when the user clicks cancel button', async () => {
      performRender();

      // wait for list to be loaded
      await waitFor(() => expect(getCounter()).toBeInTheDocument());

      // open modal
      fireEvent.click(getCustomerRowCells(1)[1]);
      expect(screen.queryByTestId('body-portal')).toBeInTheDocument();
      await waitFor(() => expect(getModal()).toBeInTheDocument());

      // provide a new budget value
      userEvent.clear(getBudgetValueInput()!);
      userEvent.type(getBudgetValueInput()!, '20000');

      // submit
      userEvent.click(getCancelButton()!);

      // wait for modal to disappear
      await waitFor(() => expect(getModal()).not.toBeInTheDocument());

      // verify if there are no changes to the list
      expect(getCustomerRowCells(1)[2].textContent).toContain('10.000,00');
      expect(getCustomerRowCells(1)[4].textContent).toContain('5.500,00');
    });

    test('should not persist value when the user presses escape key', async () => {
      performRender();

      // wait for list to be loaded
      await waitFor(() => expect(getCounter()).toBeInTheDocument());

      // open modal
      fireEvent.click(getCustomerRowCells(1)[1]);
      expect(screen.queryByTestId('body-portal')).toBeInTheDocument();
      await waitFor(() => expect(getModal()).toBeInTheDocument());

      // provide a new budget value
      userEvent.clear(getBudgetValueInput()!);
      userEvent.type(getBudgetValueInput()!, '20000');

      fireEvent.keyUp(getBudgetValueInput()!, { key: 'Escape' });

      // wait for modal to disappear
      await waitFor(() => expect(getModal()).not.toBeInTheDocument());

      // verify if there are no changes to the list
      expect(getCustomerRowCells(1)[2].textContent).toContain('10.000,00');
      expect(getCustomerRowCells(1)[4].textContent).toContain('5.500,00');
    });

    test('should not persist value then the user clicks outside of the modal', async () => {
      performRender();

      // wait for list to be loaded
      await waitFor(() => expect(getCounter()).toBeInTheDocument());

      // open modal
      fireEvent.click(getCustomerRowCells(1)[1]);
      expect(screen.queryByTestId('body-portal')).toBeInTheDocument();
      await waitFor(() => expect(getModal()).toBeInTheDocument());

      // provide a new budget value
      userEvent.clear(getBudgetValueInput()!);
      userEvent.type(getBudgetValueInput()!, '20000');

      fireEvent.click(getOverlay()!);

      // wait for modal to disappear
      await waitFor(() => expect(getModal()).not.toBeInTheDocument());

      // verify if there are no changes to the list
      expect(getCustomerRowCells(1)[2].textContent).toContain('10.000,00');
      expect(getCustomerRowCells(1)[4].textContent).toContain('5.500,00');
    });
  });
});
/*eslint-enable*/
