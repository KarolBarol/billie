import React, { useContext, useState } from 'react';
import { Button, Modal, ModalContext } from '@common/components';
import { H3, Text, ThemeProp } from '@theme';
import { toDECurrencyString } from '@common/utils';
import styled from '@emotion/styled';

import { Customer } from './Customer';

interface CustomerBudgetModalProps {
  onSubmit: (customer: Customer) => unknown;
  customer: Customer;
}

export const CustomersBudgetModal: React.FC<CustomerBudgetModalProps> = ({
  customer,
  onSubmit,
}) => {
  const [budgetValue, setBudgetValue] = useState(customer.budget);
  const [isBudgetValid, setIsBudgetValid] = useState(true);
  const { toggleIsOpened } = useContext(ModalContext);

  return (
    <Modal
      render={({ ariaDescribedBy, ariaLabelledBy }) => {
        return (
          <ModalBody>
            <H3 id={ariaLabelledBy}>Available Budget</H3>
            <Text id={ariaDescribedBy}>Provide a new budget value</Text>

            <Form
              onSubmit={(e) => {
                e.preventDefault();
                onSubmit({
                  ...customer,
                  budget: budgetValue,
                  budget_display_value: toDECurrencyString(budgetValue),
                  budget_left: budgetValue - customer.budget_spent,
                  budget_left_display_value: toDECurrencyString(
                    budgetValue - customer.budget_spent
                  ),
                });
                toggleIsOpened();
              }}
            >
              <NumberInputWrapper>
                <NumberInput
                  type="number"
                  name="budgetValue"
                  /**
                   *  According to dialog accessibility specs this is a desired behavior for a dialog
                   *  Section: Accessibility Features, Point: 2
                   *  https://www.w3.org/TR/wai-aria-practices/examples/dialog-modal/dialog.html
                   */
                  // eslint-disable-next-line jsx-a11y/no-autofocus
                  autoFocus
                  value={budgetValue}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const updatedBudget = Number(e.target.value);
                    setBudgetValue(updatedBudget);
                    setIsBudgetValid(updatedBudget > customer.budget_spent);
                  }}
                />

                {!isBudgetValid && (
                  <ErrorMessage role="alert">
                    Must be greater than a value spent (
                    {customer.budget_spent_display_value})!
                  </ErrorMessage>
                )}
              </NumberInputWrapper>

              <ButtonsContainer>
                <Button type="button" onClick={toggleIsOpened}>
                  Cancel
                </Button>
                <Button
                  type="submit"
                  variant={'primary'}
                  disabled={!isBudgetValid}
                >
                  Submit
                </Button>
              </ButtonsContainer>
            </Form>
          </ModalBody>
        );
      }}
    />
  );
};

const ModalBody = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Form = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const NumberInputWrapper = styled.div`
  position: relative;
`;

const NumberInput = styled.input<ThemeProp>(
  ({ theme }) => `
  padding: 0.2rem;
  margin: 1.5rem 0 3.5rem 0;
  width: 26rem;
  height: 3.2rem;
  border: none;
  border-bottom: ${`1px solid  ${theme.colors.black}`};
  line-height: 1.6;
  font-size: 1.4rem;
  color: ${theme.colors.black};
  caret-color: ${theme.colors.black};

  transition: all 0.25s ease-in-out;

  &:hover,
  &:focus {
    outline: none;
  }

  &:focus {
    background-color: ${theme.colors.greyLight};
  }
`
);

const ErrorMessage = styled.span<ThemeProp>(
  ({ theme }) => `
  position: absolute;
  top: 6rem;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  font-size: 1.2rem;
  color: ${theme.colors.red};
`
);

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
  button:first-of-type {
    margin-right: 1.5rem;
  }
`;
