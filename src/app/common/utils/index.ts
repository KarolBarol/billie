export const toDECurrencyString = (val: number): string =>
  val.toLocaleString('de-DE', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 2,
  });
