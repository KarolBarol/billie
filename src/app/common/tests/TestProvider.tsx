import React from 'react';
import { ModalProvider } from '@common/components';
import { ThemeProvider } from '@theme';
import { Toaster } from 'react-hot-toast';

export const TestProvider: React.FC = ({ children }) => (
  <ThemeProvider>
    <ModalProvider>{children}</ModalProvider>
    <Toaster />
  </ThemeProvider>
);
