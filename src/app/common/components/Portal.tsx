import React from 'react';
import { createPortal } from 'react-dom';

export const Portal: React.FC = ({ children }) => {
  let bodyPortal = document.getElementById('body-portal');

  if (!bodyPortal) {
    bodyPortal = document.createElement('div');
    bodyPortal.setAttribute('id', 'body-portal');
    bodyPortal.setAttribute('data-testid', 'body-portal');
    document.querySelector('body')?.appendChild(bodyPortal);
  }

  return createPortal(children, bodyPortal);
};
