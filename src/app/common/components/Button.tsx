import React, { ButtonHTMLAttributes } from 'react';
import { css } from '@emotion/react';
import { useTheme } from '@theme';

export type ButtonVariants = 'primary' | 'secondary';

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: ButtonVariants;
}

export const Button: React.FC<ButtonProps> = ({
  children,
  variant = 'secondary',
  ...props
}) => {
  const { colors } = useTheme();
  const isPrimary = variant === 'primary';

  return (
    <button
      css={css`
        padding: 1rem 1.5rem;
        border-radius: 0.4rem;
        border: ${`1px solid ${isPrimary ? 'transparent' : colors.black}`};
        font-size: 1.4rem;
        text-transform: uppercase;
        color: ${isPrimary ? colors.white : colors.black};
        background-color: ${isPrimary ? colors.primary : 'transparent'};
        cursor: pointer;

        transition: all 250ms ease-in;

        &[disabled] {
          cursor: not-allowed;
          opacity: 0.46;
        }

        &:hover {
          filter: brightness(105%);
          box-shadow: 0px 3px 1px -2px rgb(0 0 0 / 20%),
            0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%);
        }

        &:active {
          filter: brightness(95%);
          box-shadow: none;
        }
      `}
      {...props}
    >
      {children}
    </button>
  );
};
