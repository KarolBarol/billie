import React, { useCallback, useEffect, useRef, useState } from 'react';

export const useModal = (): {
  isOpened: boolean;
  setIsOpened: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const [isOpened, setIsOpened] = useState(false);
  const rootRef = useRef(document.querySelector('#root'));

  const escKeyUpHandler = useCallback(
    (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        setIsOpened(false);
      }
    },
    [setIsOpened]
  );

  useEffect(() => {
    isOpened
      ? rootRef.current?.setAttribute('inert', 'true')
      : rootRef.current?.setAttribute('inert', 'false');
  }, [isOpened]);

  useEffect(() => {
    document.addEventListener('keyup', escKeyUpHandler);
    return () => {
      document.removeEventListener('keyup', escKeyUpHandler);
    };
  }, [escKeyUpHandler]);

  return { isOpened, setIsOpened };
};
