import React, { createContext, useContext } from 'react';
import { ThemeProp } from '@theme';
import FocusTrap from 'focus-trap-react';
import styled from '@emotion/styled';

import { Portal } from '../Portal';
import { useModal } from './useModal';

export const ModalContext = createContext<{
  isOpened: boolean;
  toggleIsOpened: () => unknown;
}>({
  isOpened: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  toggleIsOpened: () => {},
});

export const ModalProvider: React.FC = ({ children }) => {
  const { isOpened, setIsOpened } = useModal();
  return (
    <ModalContext.Provider
      value={{
        isOpened,
        toggleIsOpened: () => setIsOpened(!isOpened),
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};

export const Modal: React.FC<{
  render: (data: {
    ariaLabelledBy: string;
    ariaDescribedBy: string;
  }) => React.ReactChild;
}> = ({ render }) => {
  const { isOpened, toggleIsOpened } = useContext(ModalContext);
  const ariaLabelledBy = 'modalTitle';
  const ariaDescribedBy = 'modalDescription';

  return (
    <Portal>
      {isOpened && (
        <FocusTrap>
          <Container>
            <Overlay data-testid="modal--overlay" onClick={toggleIsOpened} />
            <Wrapper
              role="dialog"
              aria-labelledby={ariaLabelledBy}
              aria-describedby={ariaDescribedBy}
            >
              <Body>
                {render({
                  ariaLabelledBy,
                  ariaDescribedBy,
                })}
              </Body>
            </Wrapper>
          </Container>
        </FocusTrap>
      )}
    </Portal>
  );
};

const Container = styled.div``;

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.16);
`;

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const Body = styled.div<ThemeProp>(
  ({ theme }) => `
  padding: 2rem;
  border-radius: 0.4rem;
  width: 42rem;
  height: 28rem;
  background-color: ${theme.colors.white};
`
);
