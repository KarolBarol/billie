import React from 'react';
import { ThemeProp } from '@theme';
import Skeleton from 'react-loading-skeleton';
import styled from '@emotion/styled/macro';

export interface TableCol {
  label: string;
  propName: string;
  width?: string;
  maxWidth?: string;
}

interface TableProps {
  cols: TableCol[];
  data: Record<string, string | number>[];
  isLoading: boolean;
  onRowClick?: (e: { rowIdx: number }) => unknown;
}

export const Table: React.FC<TableProps> = ({
  cols,
  data,
  isLoading,
  onRowClick,
}) => {
  const renderHeaders = (): JSX.Element[] => {
    return cols.map(
      ({ label, propName, width = 'auto', maxWidth = 'auto' }) => (
        <HeaderCell key={propName} scope="column" style={{ width, maxWidth }}>
          {!isLoading ? label : <Skeleton />}
        </HeaderCell>
      )
    );
  };

  const renderRows = (): JSX.Element[] => {
    if (isLoading) {
      return new Array(3).fill(null).map((customer, idx) => (
        <Row key={idx}>
          {cols.map(({ propName }) => (
            <Cell key={propName}>
              <Skeleton />
            </Cell>
          ))}
        </Row>
      ));
    }
    return data.map((item, idx) => (
      <Row key={idx} onClick={() => onRowClick && onRowClick({ rowIdx: idx })}>
        {cols.map(({ propName, width = 'auto', maxWidth = 'auto' }) => (
          <Cell key={propName} style={{ width, maxWidth }}>
            {item[propName]}
          </Cell>
        ))}
      </Row>
    ));
  };

  return (
    <ResponsiveTableWrapper>
      <StyledTable cellSpacing={0}>
        <thead>
          <Row>{renderHeaders()}</Row>
        </thead>
        <tbody>{renderRows()}</tbody>
      </StyledTable>
    </ResponsiveTableWrapper>
  );
};

const Row = styled.tr``;

const Cell = styled.td`
  transition: all 200ms ease-in;
`;

const HeaderCell = styled.th`
  font-size: 1.2rem;
  text-transform: uppercase;
`;

const ResponsiveTableWrapper = styled.div`
  overflow-x: auto;
`;

const StyledTable = styled.table<ThemeProp>(
  ({ theme }) => `
  width: 100%;

  caption {
    visibility: hidden;
  }

  ${Row}:hover {
    ${Cell} {
      background-color: ${theme.colors.greyLight};
    }
  }

  ${HeaderCell},
  ${Cell} {
    padding: 1.5rem 1.5rem 1.5rem 0.5rem;
    border-bottom: 1px solid ${theme.colors.grey};
    text-align: left;
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
  }
`
);
