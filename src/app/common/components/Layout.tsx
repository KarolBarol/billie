import React from 'react';
import styled from '@emotion/styled';
import { ThemeProp } from '@theme';

import { Header, Sidebar } from '.';

interface LayoutProps {
  title: string;
  description: string;
}

export const Layout: React.FC<LayoutProps> = ({ children, ...props }) => {
  return (
    <TwoColumns>
      <Sidebar />
      <Container>
        <Header {...props} />
        {children}
      </Container>
    </TwoColumns>
  );
};

const TwoColumns = styled.div`
  height: 100%;
  display: grid;
  grid-template-columns: 6.4rem 1fr;
`;

const Container = styled.div<ThemeProp>(
  ({ theme }) => `
  grid-column: 1 / 3;
  padding-top: 6.4rem;
  height: 100%;
  display: flex;
  flex-direction: column;
  flex: 1;

  @media (min-width: ${theme.bp.lg}) {
    grid-column: 2 / 3;
    padding-top: 0;
  }
`
);
