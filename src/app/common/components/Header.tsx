import React from 'react';
import styled from '@emotion/styled';
import { H1, ThemeProp } from '@theme';

interface HeaderProps {
  title: string;
  description: string;
}

export const Header: React.FC<HeaderProps> = ({ title, description }) => (
  <HeaderWrapper>
    <H1>{title}</H1>
    <p>{description}</p>
  </HeaderWrapper>
);

const HeaderWrapper = styled.header<ThemeProp>(
  ({ theme }) => `
  height: 30rem;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${theme.colors.grey};

  @media (min-width: ${theme.bp.lg}) {
    padding: 0 8.6rem;
    height: 36rem;
  }
`
);
