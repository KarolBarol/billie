import React from 'react';
import styled from '@emotion/styled';
import { ThemeProp } from '@theme';

export const Sidebar: React.FC = () => (
  <SidebarWrapper>
    <Brand>B</Brand>
  </SidebarWrapper>
);

const SidebarWrapper = styled.div<ThemeProp>(
  ({ theme }) => `
  position: fixed;
  top: 0;
  width: 100%;
  height: 6.4rem;
  grid-column: 1 / 3;
  background-color: ${theme.colors.secondary};

  @media (min-width: ${theme.bp.lg}) {
    width: 6.4rem;
    height: 100%;
    grid-column: 1 / 2;
  }
`
);

const Brand = styled.div<ThemeProp>(
  ({ theme }) => `
  height: 6.4rem;
  width: 6.4rem;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 3.6rem;
  color: ${theme.colors.white};
`
);
