import React from 'react';
import { ModalProvider } from '@common/components';
import { ThemeProvider } from '@theme';
import { Toaster } from 'react-hot-toast';
import Customers from '@customers';
export const App: React.FC = () => (
  <ThemeProvider>
    <ModalProvider>
      <Customers />
      <Toaster />
    </ModalProvider>
  </ThemeProvider>
);
